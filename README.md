# Custis-test-app
FileGeneratorApp - генерирует файлы <br>
DocSortApplication - сортирует файлы <br>

Сначала запускать FileGeneratorApp, после DocSortApplication.<br>

При запуске FileGeneratorApp,в проекте сгенерируется папка textfiles.<br>
Внутри будут еще две папки in и out.<br> 
text.txt в in - исходный файл<br>
left.txt и right.txt - промежуточные файлы, в них происходит сортировка <br>
По результату сортировки один из файлов (left.txt или right.txt) переименуется в result.txt, в котором и будет окончательный вариант.  

Алгоритм сортировки слиянием
