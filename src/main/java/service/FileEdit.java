package service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileEdit {

    public void renameFile(String result) {
        File file = new File(result);
        file.renameTo(new File("textfiles/out/result.txt"));
    }

    public void clearFile(String clearResource) {
        try {
            Files.deleteIfExists(Paths.get(clearResource));
            Files.createFile(Paths.get(clearResource));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
