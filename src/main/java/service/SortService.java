package service;

import java.nio.file.Paths;

public class SortService {

    private final MyFileWriter resultFileWriter = new MyFileWriter();
    private final MyFileReader myFileReader = new MyFileReader();
    private final FileEdit fileEdit = new FileEdit();
    private String left = "textfiles/out/left.txt";
    private String right = "textfiles/out/right.txt";
    private String result = "textfiles/out/result.txt";

    void sort() {

        long size = myFileReader.getSize(left);

        for (long j = 1; j < size; j = 2 * j) {
            for (long i = 0; i < size; i = i + 2 * j) {
                merge(left, i, Math.min(i + j, size), Math.min(i + 2 * j, size), right);
            }
            String tmp;
            tmp = left;
            left = right;
            right = tmp;
            fileEdit.clearFile(tmp);
            result = left;
        }
        fileEdit.renameFile(result);
    }

    void merge(String left, long iLeft, long iRight, long iEnd, String right) {

        long i = iLeft;
        long j = iRight;

        for (long k = iLeft; k < iEnd; k++) {
            String leftString = myFileReader.getString(left, i);
            String rightString = myFileReader.getString(left, j);
            int i1 = leftString.compareTo(rightString);
            if (i < iRight && (j >= iEnd || i1 <= 0)) {
                resultFileWriter.fileWriter(Paths.get(right), leftString + System.lineSeparator());
                i++;
            } else {
                resultFileWriter.fileWriter(Paths.get(right), rightString + System.lineSeparator());
                j++;
            }
        }
    }
}
