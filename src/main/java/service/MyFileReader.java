package service;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class MyFileReader {

    public String getString(String inputResourcePath, long stringNumber) {
        String string = null;
        try (BufferedReader reader = Files.newBufferedReader(Paths.get(inputResourcePath).normalize())) {
            for (long i = 0; i <= stringNumber; i++) {
                string = reader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return string == null ? "" : string;
    }

    public long getSize(String inputResourcePath) {
        long size = 0;
        try (BufferedReader reader = Files.newBufferedReader(Paths.get(inputResourcePath).normalize())) {
            while (reader.readLine() != null) {
                size++;
            }
        } catch (IOException e) {
            System.out.println("Сначала нужно запустить генерацию файлов");
        }
        return size;
    }
}
