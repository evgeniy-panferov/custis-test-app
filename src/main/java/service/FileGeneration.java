package service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Random;


public class FileGeneration {

    private MyFileWriter fileWriter = new MyFileWriter();
    private final String inPath = "textfiles/in";
    private final String outPath = "textfiles/out";

    public int generate(int countLine, int countChar) {

        deleteFileIfExists();
        createFile();

        Random r = new Random();
        StringBuilder stringBuilder = new StringBuilder();
        String alphabet = "1234567890abcdefghijklmnopqrstuvwxyz";

        for (int i = 0; i < countLine; i++) {
            int rnd = r.ints(1, countChar + 1).findFirst().getAsInt();
            for (int j = 0; j < rnd; j++) {
                stringBuilder.append(alphabet.charAt(r.nextInt(alphabet.length())));
            }

            String lastStringWithoutSpace = i == countLine - 1 ? stringBuilder.toString() :
                    stringBuilder.append(System.lineSeparator()).toString();

            fileWriter.fileWriter(Paths.get(inPath + "/text.txt"), lastStringWithoutSpace);
            fileWriter.fileWriter(Paths.get(outPath + "/left.txt"), lastStringWithoutSpace);// -1 проход по циклу в sortService
            stringBuilder.setLength(0);
        }
        return countLine;
    }

    private void createFile() {
        File file = new File(inPath);
        File file1 = new File(outPath);
        file.mkdirs();
        file1.mkdirs();
        try {
            Files.createFile(Paths.get(inPath + "/text.txt").normalize());
            Files.createFile(Paths.get(outPath + "/left.txt").normalize());
            Files.createFile(Paths.get(outPath + "/right.txt").normalize());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void deleteFileIfExists() {
        try {
            Files.deleteIfExists(Paths.get("textfiles/out/result.txt").normalize());
            Files.deleteIfExists(Paths.get("textfiles/in/text.txt").normalize());
            Files.deleteIfExists(Paths.get("textfiles/out/right.txt").normalize());
            Files.deleteIfExists(Paths.get("textfiles/out/left.txt").normalize());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
